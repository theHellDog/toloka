defmodule Toloka.Worker do
  use GenServer

  @name TW

  ## Client API

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts ++ [name: TW])
  end

  def get_topic(topic_id) do
    GenServer.call(@name, {:topic_id, topic_id})
  end

  def get_stats do
    GenServer.call(@name, :get_stats)
  end

  def reset_stats do
    GenServer.cast(@name, :reset_stats)
  end

  def stop do
    GenServer.cast(@name, :stop)
  end

  ## Server Callbacks

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_call({:topic_id, topic_id}, _from, stats) do
    case data_of(topic_id) do
      {:ok, temp} ->
        new_stats = update_stats(stats, topic_id)
        {:reply, temp, new_stats}

      _ ->

        {:reply, :error, stats}  
    end
  end

  def handle_call(:get_stats, _from, stats) do
    {:reply, stats, stats}  
  end

  def handle_cast(:reset_stats, _stats) do
    {:noreply, %{}}
  end

  def handle_cast(:stop, stats) do
    {:stop, :normal, stats}
  end

  def terminate(reason, stats) do
    # We could write to a file, database etc
    IO.puts "server terminated because of #{inspect reason}"
    inspect stats
    :ok
  end

  def handle_info(msg, stats) do
    IO.puts "received #{inspect msg}"
    {:noreply, stats}
  end

  ## Helper Functions

  defp data_of(topic_id) do
    url_for_topic(topic_id) |> HTTPoison.get 
  end

  defp url_for_topic(topic_id) do
    "https://toloka.to/t#{topic_id}"
  end

  defp update_stats(old_stats, location) do
    case Map.has_key?(old_stats, location) do
      true ->
        Map.update!(old_stats, location, &(&1 + 1))
      false ->
        Map.put_new(old_stats, location, 1)
    end
  end

end
