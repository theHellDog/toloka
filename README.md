1. mix deps.get
2. mix compile
3. iex -S mix
4.
  ```elixir
    {:ok, pid} = Toloka.Worker.start_link

    Toloka.Worker.get_topic(%topic_id_here%)

    Toloka.Worker.get_stats #counter of gets
  ```

# Toloka

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `toloka` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:toloka, "~> 0.1.0"}]
    end
    ```

  2. Ensure `toloka` is started before your application:

    ```elixir
    def application do
      [applications: [:toloka]]
    end
    ```

